// This file is part of Ecphoneme
//
// Copyright (C) 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
//
// Ecphoneme is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ecphoneme is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Ecphoneme.  If not, see
// <http://www.gnu.org/licenses/>.


const { Cc, Ci, Cr } = require("chrome");

var system = require("sdk/system");
var events = require("sdk/system/events");
// var { MatchPattern } = require("sdk/util/match-pattern");

var tabs = require("sdk/tabs");
var panel = require("sdk/panel");
var utils = require("sdk/window/utils");
var qs = require("sdk/querystring");
var simple_prefs = require("sdk/simple-prefs");
var prefs = simple_prefs.prefs;
var self = require("sdk/self");
var data = self.data;

var location = require('./location.js');

var mozilla_id="{ec8030f7-c20a-464f-9b0e-13a3a9e97384}"

var has_widgets = false; 

// Firefox 29 deprecated widgets.
if ( system.id == mozilla_id && 
     parseInt(system.version.split(".")[0]) < 29) {
    has_widgets = true;
}

var widgets, ui;

if (has_widgets) {
    widgets = require("sdk/widget");
} else {
    ui = require("sdk/ui");
}

var toolbar_button_label_enable = "Enable Ecphoneme search";
var toolbar_button_label_disable = "Disable Ecphoneme search";
var toolbar_button_icon_enable = "./ecphoneme-toolbar-icon-16.png";
var toolbar_button_icon_disable = "./ecphoneme-toolbar-icon-disabled-16.png";


function render_help() {
    var tab = tabs.open({ 
	url: data.url("./help.html"),
	onReady: render_help_websites
    });
}
    

function render_help_websites(tab) {
    var worker = tab.attach({
	contentScriptFile: data.url("./help.js"),
    });

    worker.port.emit("websites_data", { 
	websites: location.websites(),
	separator: prefs.search_indicator,
    });
}

function toolbar_button_pressed(state) {
    var icon = this.icon || this.contentURL;

   if (icon.match(/.*disabled.*/)) {
	prefs.enable_search = "1";
	this.label = toolbar_button_label_disable;
	icon = toolbar_button_icon_enable; 
    } else {
	prefs.enable_search = "0";
	this.label = toolbar_button_label_enable;
	icon = toolbar_button_icon_disable; 
    }

    if (this.icon) {
	this.icon = icon;
    } else {
	this.contentURL = data.url(icon);
    }
}

function capture_url_listener (event) 
{ 
    // The Ecphoneme search is disabled
    if (!parseInt(prefs.enable_search)) {
	return;
    }

    var channel = event.subject.QueryInterface(Ci.nsIHttpChannel);
    var url = event.subject.URI.spec;
    var sep = prefs.search_indicator;
    // Matches all possible cases
    // ?q=!wikipedia+JavaScript
    // !wt program 
    // SDK examples !sp firefox
    var sep_re = 
	new RegExp ("(.*)(\\\s+|=|\\\+|\\\%20|^)(("+
		    qs.escape(sep)+"|"+sep+")([a-zA-Z0-9]+))(\\\s+|\\\+|\\\%20|$|&)(.*)");

    var sep_match = url.match(sep_re);

    if (!sep_match || !sep_match[sep_match.length-3]) {
	return;
    }

    var l = sep_match.length;
    var keyword = sep_match[l-3];

    var query_re =
	new RegExp ("(.*)(\\\s+|=|\\\+|\\\%20)(("+
		    sep+")([a-zA-Z0-9]+))(\\\s+|\\\+|\\\%20|$)(.*)");

    var query = url.slice((url.indexOf("?")+1));
    query = qs.parse(query);

    var user_keywords = null;
    var qs_re = new RegExp (sep+keyword);
    var clear = new RegExp( 
	"(\\\+)*"+sep+keyword+"(\\\+)*",
	"g"
    );

    if (query.q && qs_re.test(query.q)) {
	user_keywords = query.q.replace(clear,"+");
    } else {
	for (var q in query) {
	    if (qs_re.test(query[q])) {
		user_keywords = query[q].replace(clear,"+");
		break;
	    }
	}
    }

    if (!user_keywords) {
	return;
    }

    var ws_data = location.website_data_by_keyword(keyword);

    if (!ws_data) {
	return;
    }

    channel.cancel(Cr.NS_BINDING_ABORTED);

    if (keyword == "help") {
	render_help();
	return;
    }


    var url = location.construct_url(ws_data.url, user_keywords);

    var gBrowser = utils.getMostRecentBrowserWindow().gBrowser;
    var domWin = channel.notificationCallbacks.getInterface(Ci.nsIDOMWindow);
    var browser = gBrowser.getBrowserForDocument(domWin.top.document);
    browser.loadURI(url);
}



exports.main = function() {
    var button, label, icon;

    if (parseInt(prefs.enable_search)) {
	label = toolbar_button_label_disable;
	icon = toolbar_button_icon_enable;
    } else {
	label = toolbar_button_label_enable;
	icon = toolbar_button_icon_disable;
    }

    if (!has_widgets) {
	button = ui.ActionButton({
	    id: "toggle-ecphoneme",
	    label: label,
	    icon: icon,
	    disabled: false,
	    onClick: toolbar_button_pressed
	});
    } else {
	button = widgets.Widget({
	    id: "toggle-ecphoneme",
	    label: label,
	    disabled: false,
	    contentURL: data.url(icon),
	    onClick: toolbar_button_pressed
	});
    }

    location.init();
    events.on("http-on-modify-request", capture_url_listener);
    simple_prefs.on("supported_websites", render_help);
}
