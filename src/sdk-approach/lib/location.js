// This file is part of Ecphoneme
//
// Copyright (C) 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
//
// Ecphoneme is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ecphoneme is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Ecphoneme.  If not, see
// <http://www.gnu.org/licenses/>.

var ss = require("sdk/simple-storage");
var keywords = new Object();
var urls = new Object();
var websites = new Array();

function data_index_by_keyword(keyword) {
    if (!keyword) {
	return null;
    }

    var have_keyword = keywords[keyword];

    if (!isNaN(have_keyword)) {
	return have_keyword;
    }

    return null;
}

function data_index_by_url(url) {
    if (!url) {
	return null;
    }

    var have_url = urls[url];

    if (!isNaN(have_url)) {
	return have_url;
    }

    return null;
}

function website_data_by_index(idx) {
    if (isNaN(idx)) {
	return null;
    }

    return websites[idx];;
}

function website_data_by_url(url) {
    var url_idx = data_index_by_url(url);

    return website_data_by_index(url_idx);
}

function website_data_by_keyword(keyword) {
    var keyword_idx = data_index_by_keyword(keyword);

    return website_data_by_index(keyword_idx);
}

function construct_url(url, user_keywords) {
    url = url.replace(/%s/g, user_keywords)
    
    return url;
}


function websites_data() {
    return websites;
}

function init_storage() {
    // FOR DEBUG PURPOSES ONLY !!!
    // DELETES STORAGE !!!! 
    // UNCOMMENT WITH CAUTION !!!!
    
    ss.storage.keyword_index = new Object();
    ss.storage.url_index = new Object();
    ss.storage.websites = new Array();

    if (!ss.storage.keyword_index) {
	ss.storage.keyword_index = new Object();
    }

    if (!ss.storage.website_index) {
	ss.storage.url_index = new Object();
    }

    if (!ss.storage.websites) {
	ss.storage.websites = new Array();
    }

    for (var i=0,l=_init_websites.length; i<l; i++) {
	var loc = _init_websites[i];
	var url = loc.url;
	var websites_index = null;

	// The first keyword must be alwyas present. If it is not this
	// is the first run.
	if (isNaN(ss.storage.keyword_index[loc.keywords[0]])) {
	    ss.storage.websites.push(_init_websites[i]);
	    websites_index = ss.storage.websites.length-1;
	} else {
	    websites_index = ss.storage.keyword_index[loc.keywords[0]];
	}
	
	// Do not overide existing data. The user might have changed
	// it
	if (isNaN(ss.storage.url_index[url])) {
	    ss.storage.url_index[url] = websites_index;
	}
	
	for (var k=0,lk=loc.keywords.length; k<lk; k++) {
	    var keyword = loc.keywords[k];

	    // Do not overide existing data. The user might have changed
	    // it
	    if (isNaN(ss.storage.keyword_index[keyword])) {
		ss.storage.keyword_index[keyword] = websites_index;
	    }
	}
    }

    // Not needed anymore
    delete _init_websites;

    // Load data into memory; reduce IO
    websites = ss.storage.websites;

    // Load indexes into memory; reduce IO
    urls = ss.storage.url_index;
    keywords = ss.storage.keyword_index;
}

var _init_websites = [
    {
	"keywords": [ "help" ],
	"label": "Ecphoneme help page",
	"url": "This page"
    },
    {
	"keywords": [ "ecphoneme", "home", "homepage" ],
	"label": "Ecphoneme home page",
	"url": "http://e-valkov.org/ecphoneme"
    },
    {
	"keywords": [ "sp", "s", "startpage" ],
	"label": "Startpage",
	"url": "https://startpage.com/do/search?q=%s"
    },
    {
	"keywords": [ "si", "spi", "img", "images" ],
	"label": "Startpage Images",
	"url": "https://startpage.com/do/search?cat=pics&cmd=process_search&query=%s"
    },

    {
	"keywords": [ "sv", "spv", "video", "vid" ],
	"label": "Startpage Videos",
	"url": "https://startpage.com/do/search?cat=video&cmd=process_search&query=%s"
    },

    {
	"keywords": [ "ddg", "d", "duckduckgo" ],
	"label": "DuckDuckGo",
	"url": "https://duckduckgo.com/?q=%s"
    },
    {
	"keywords": [ "w", "wiki", "wk", "wikipedia" ],
	"label": "Wikipedia (EN)",
	"url": "https://en.wikipedia.org/wiki/Special:Search?search=%s"
    },
    {
	"keywords": [ "wt", "wte", "wiktionary" ],
	"label": "Wiktionary (EN)",
	"url": "https://en.wiktionary.org/wiki/Special:Search?search=%s"
    },
    {
	"keywords": [ "g", "ggl", "google" ],
	"label": "Google",
	"url": "https://www.google.com/search?q=%s"
    },
    {
	"keywords": [ "gi", "ggli", "gimages", "googleimages" ],
	"label": "Google Images",
	"url": "https://google.com/search?tbm=isch&q=%s"
    },
    {
	"keywords": [ "b", "bing" ],
	"label": "Bing",
	"url": "https://bing.com/?q=%s"
    },
    {
	"keywords": [ "bi", "bingimages" ],
	"label": "Bing Images",
	"url": "https://www.bing.com/images/search?q=%s"
    },
    {
	"keywords": [ "addons", "firefox" ],
	"label": "Firefox addons",
	"url": "https://addons.mozilla.org/en-US/firefox/search/?q=%s"
    },
    {
	"keywords": [ "metacpan", "mcp", "mcpan"],
	"label": "MetaCPAN",
	"url": "https://metacpan.org/search?q=%s"
    },
    {
	"keywords": [ "cpan" ],
	"label": "CPAN",
	"url": "http://search.cpan.org/search?query=%s"
    },
    {
	"keywords": [ "pm", "perlmonks" ],
	"label": "Perl Monks",
	"url": "http://perlmonks.org/?node=%s"
    },
    {
	"keywords": [ "imdb" ],
	"label": "IMDB",
	"url": "http://www.imdb.com/find?s=all&q=%s"
    },
    {
	"keywords": [ "yc", "yacy" ],
	"label": "YaCy P2P Demo-Search",
	"url": "http://search.yacy.net/yacysearch.html?query=%s"
    },
    {
	"keywords": [ "yt", "youtube" ],
	"label": "YouTube",
	"url": "https://www.youtube.com/results?search_query=%s"
    },
    {
	"keywords": ["vm", "vimero"],
	"label": "Vimeo",
	"url": "https://vimeo.com/search?q=%s"
    }
];

exports.init = init_storage;
exports.data_index_by_keyword = data_index_by_keyword;
exports.data_index_by_url = data_index_by_url;
exports.website_data_by_index = website_data_by_index;
exports.website_data_by_url = website_data_by_url;
exports.website_data_by_keyword = website_data_by_keyword;
exports.construct_url = construct_url;
exports.websites = websites_data;