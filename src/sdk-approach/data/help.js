// This file is part of Ecphoneme
//
// Copyright (C) 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
//
// Ecphoneme is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ecphoneme is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Ecphoneme.  If not, see
// <http://www.gnu.org/licenses/>.


self.port.on("websites_data", function(websites_data) {
    var ws_wrapper = document.getElementById("websites");
    var ws_list = websites_data.websites;
    var sep =  websites_data.separator;

    if (!ws_list || !ws_wrapper) {
	return;
    }

    var dl = document.createElement("dl");

    var l=ws_list.length;
    for (var wd=0; wd<l; wd++) {
	var ws = ws_list[wd];

	var dt = document.createElement("dt");
	dt.textContent = ws.label;
	dl.appendChild(dt);

	var dd = document.createElement("dd");

	var dl_url = document.createElement("dl");
	var dt_url = document.createElement("dt");
	dt_url.textContent = "URL";

	var dd_url = document.createElement("dd");
	dd_url.textContent = ws.url.replace(/%s/g, "");
	dl_url.appendChild(dt_url);
	dl_url.appendChild(dd_url);
	dd.appendChild(dl_url);

	var kw_dl = document.createElement("dl");

	var kw_dt = document.createElement("dt");
	kw_dt.textContent = "Keywords";
	kw_dl.appendChild(kw_dt);

	var kw_dd = document.createElement("dd");
	var kw_ul = document.createElement("ul");

	for (var k=0,ll=ws.keywords.length; k<ll; k++) {
	    var li = document.createElement("li");
	    li.textContent = sep+ws.keywords[k];
	    kw_ul.appendChild(li);
	}

	kw_dd.appendChild(kw_ul);
	kw_dl.appendChild(kw_dd)
	dd.appendChild(kw_dl);
	dl.appendChild(dd);
    }

    ws_wrapper.appendChild(dl); 
});
