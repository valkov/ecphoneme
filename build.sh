#!/bin/bash

# This file is part of Ecphoneme
#
# Copyright (C) 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
#
# Ecphoneme is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ecphoneme is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Ecphoneme.  If not, see
# <http://www.gnu.org/licenses/>.



# tar.gz for 1.16 is a broken tar archive
mozilla_sdk=addon-sdk-1.17
mozilla_sdk_archive=zip
mozilla_sdk_url=https://ftp.mozilla.org/pub/mozilla.org/labs/jetpack/${mozilla_sdk}.${mozilla_sdk_archive}

if [ ! -f ${mozilla_sdk}/bin/cfx ];
then
    echo "Downloading Mozilla Add-on SDK";
    wget -c $mozilla_sdk_url ;
    echo "Unapacking Mozilla Add-on SDK";
    unzip -qo $mozilla_sdk 
    rm ${mozilla_sdk}.${mozilla_sdk_archive}
fi

if [ ! -f ${mozilla_sdk}/bin/cfx ];
then
    echo "Unable to use Add-on SDK. Giving up."
    exit 1;
fi

mkdir packages/ 2> /dev/null
rm -rf packages/* 

cp -a src/sdk-approach packages/ecphoneme
cp COPYING* AUTHORS README packages/ecphoneme
cp data/* packages/ecphoneme/data
cp -a ${mozilla_sdk} packages/ecphoneme

p_dir=$PWD
cd packages/ecphoneme

./${mozilla_sdk}/bin/cfx xpi

cd $p_dir

version=$(grep '"version":' \
    packages/ecphoneme/package.json|cut -d '"' -f4);


mv packages/ecphoneme/ecphoneme.xpi packages/ecphoneme-${version}.xpi

if [ -f packages/ecphoneme-${version}.xpi ];
then
    echo "Package available in packages/ecphoneme-${version}.xpi";
fi
